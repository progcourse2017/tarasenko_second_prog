#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	float x;
	float y;
	cout << "Введите значение аргумента" << endl;
	cin >> x;
	if ((x < -2) || (x > 2))
	{
		y = 0;
	}
	else if ((x >= -2) && (x < -1))
	{
		y = -4 * x - 8;
	}
	else if ((x >= -1) && (x < 1))
	{
		y = 4 * x;
	}
	else if ((x >= 1) && (x < 2))
	{
		y = -4 * x + 8;
	}
	cout << "Функция равна " << y << endl;
	system("pause");
    return 0;
}

